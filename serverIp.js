const config = require('config');
const cors = require('cors')
const { networkInterfaces } = require('os');
const express = require('express');
const http = require('http');
const parser = require('fast-xml-parser');

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

// 

const app = express()
app.use(express.static('public'))
const portExpress = config.get('DefaultPort');

const nets = networkInterfaces();
const results = Object.create(null); // or just '{}', an empty object
let localIpAddress = "";
for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
        // skip over non-ipv4 and internal (i.e. 127.0.0.1) addresses
        if (net.family === 'IPv4' && !net.internal) {
            if (!results[name]) {
                results[name] = [];
            }

            results[name].push(net.address);
        }
    }
}

// if (results && results.Ethernet) {
//     localIpAddress = results.Ethernet[0];
// }

localIpAddress = results.Ethernet[0];


app.listen(portExpress, function () {
    console.log(`App listening on port ${portExpress}! Go to http://localhost:${portExpress}/`)
})

let prevPlateObj = {};

app.use(cors());
app.get('/plates', (req, res) => {

    const requestQuery = req.query;
    const dateYear = requestQuery ? requestQuery.date : "";
    const getLatest = requestQuery && (requestQuery.latest == 'true');

    if (!isNaN(dateYear)) {

        var data = "<AfterTime><picTime>" + dateYear + "</picTime></AfterTime>";
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.addEventListener("readystatechange", function () {
            try {
                if (this.readyState === 4) {
                    //Sending Data Back To Fe
                    const jsonObj = parser.parse(this.responseText);
                    let platesLenth = jsonObj.Plates.Plate.length;
                    let currentLastPlateObj = jsonObj.Plates.Plate[platesLenth - 1];

                    if (JSON.stringify(prevPlateObj) !== JSON.stringify(currentLastPlateObj)) {
                        prevPlateObj = currentLastPlateObj;
                        res.send(prevPlateObj);

                    } else if (getLatest) {
                        const jsonObj = parser.parse(this.responseText);
                        let platesLenth = jsonObj.Plates.Plate.length;
                        let currentLastPlateObj = jsonObj.Plates.Plate[platesLenth - 1];
                        res.send(currentLastPlateObj);

                    } else {
                        res.send(JSON.stringify({ "plateNumber": "9999999" }));
                    }
                }
            } catch (err) {
                res.send(JSON.stringify({ "error": err.toString() }));
            }
        });

        const cameraIP = config.get('CameraIP');
        const username = config.get('Username');
        const pass = config.get('Password');

        let cameraAddress = "http://" + cameraIP + "/ISAPI/Traffic/channels/1/vehicleDetect/plates";
        xhr.open("POST", cameraAddress);
        let headerAuth = "Basic " + new Buffer(username + ':' + pass).toString('base64');
        xhr.setRequestHeader("Authorization", headerAuth);
        xhr.setRequestHeader("Content-Type", "text/plain");
        xhr.send(data);
    } else {
        res.send(JSON.stringify({ "error": "Not correct date" }));
    }
});