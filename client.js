const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const ByteLength = require('@serialport/parser-byte-length')
const path = "COM2"
const port = new SerialPort(path, { baudRate: 9600, dataBits: 8, stopBits: 1, parity: 'none' })

const parser = port.pipe(new ByteLength({ length: 8 }))

parser.on('data', line => console.log(`> ${line}`))
port.write('100\n')
//> ROBOT ONLINE