var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
    name: 'Westernacher IP Camera Solution',
    description: 'Westernacher Camera Plates Solution That serves data on port 8892',
    script: 'serverIp.js',
    nodeOptions: [
        '--harmony',
        '--max_old_space_size=4096'
    ],
    env: [
        { name: "NODE_ENV", value: "production" },
        { name: "NODE_CONFIG_DIR", value: "config" }
    ]
    //, workingDirectory: '...'
    //, allowServiceLogon: true
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', function () {
    svc.start();
});

svc.install();